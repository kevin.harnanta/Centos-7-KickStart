#!/bin/bash
# Configuring database for Moodle
echo -e "CREATE DATABASE moodle DEFAULT CHARACTER SET UTF8 COLLATE utf8_unicode_ci;\n" | mysql --user=root --password='T@nse2301789'
echo -e "CREATE USER 'moodleuser'@'localhost' IDENTIFIED BY 'T@nse2301888';\n" | mysql --user=root --password='T@nse2301789'
echo -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO 'moodleuser'@'localhost';\n" | mysql --user=root --password='T@nse2301789'
echo -e "ALTER USER 'moodleuser'@'localhost' IDENTIFIED WITH mysql_native_password BY 'T@nse2301789';\n" | mysql --user=root --password=T@nse2301789 --host=localhost
echo -e "FLUSH PRIVILEGES;\n" | mysql --user=root --password='T@nse2301789'
# Instaling moodle with CLI
php72 /var/www/html/moodle/admin/cli/install.php --chmod=2777 --lang=en --wwwroot=http://moodle.ku --dbtype=mysqli --dbhost=localhost --dbuser=moodleuser --dbpass=T@nse2301789 --dbport=3306 --prefix=mdl_ --fullname=Blibli.com --shortname=Blibli --summary=Blibli --adminuser=admin --adminpass=12345678 --adminemail=admin@example.com --non-interactive --agree-license 
# Give the read permission to Other
chmod o+r /var/www/html/moodle/config.php
# Change the owner of moodle file to nginx
chown -R nginx:nginx /var/www/html/moodledata
chown -R nginx:nginx /var/www/html/moodle
# Allow the Nginx to read and write the file on directory
semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/moodle(/.*)?'
restorecon -Rv '/var/www/html/moodle/'
semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/moodledata(/.*)?'
restorecon -Rv '/var/www/html/moodledata/'
# Disable the custom service to not run at startup
systemctl disable reset_password.service
systemctl disable moodle.service 
# Remove custom service and script executeable on folder blibli
rm /etc/systemd/system/reset_password.service
rm /etc/systemd/system/moodle.service
rm -rf /opt/blibli
rm -rf /opt/rh
