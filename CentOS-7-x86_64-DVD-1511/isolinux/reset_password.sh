#!/bin/bash
tmp=$(grep 'temporary password' /var/log/mysqld.log | awk '{print $13}') # Grab the temporary root password created by MySQL
echo -e "ALTER USER 'root'@'localhost' IDENTIFIED BY 'T@nse2301789';\n"  | mysql --user=root --password=$tmp --host=localhost --connect-expired-password # Reset the password
echo -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'T@nse2301789';\n" | mysql --user=root --password=T@nse2301789 --host=localhost # Enable MySQL native password for root user
echo -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');\n" | mysql --user=root --password=T@nse2301789 --host=localhost # Only allow root user login from localhost  
echo -e "DROP DATABASE IF EXISTS test;\n" | mysql --user=root --password=T@nse2301789 --host=localhost # Drop database 'test' if exist
